<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) { return;
}

/**
 * Action de modif de l'échéance d'un abonnement
 * @param string $arg
 * @return ?array
 */
function action_modifier_echeance_abonnement_dist($arg = null) {
	if (is_null($arg)) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}

	$args = explode('/', $arg, 3);
	if (count($args) < 3) {
		return null;
	}

	$id_abonnement = array_shift($args);
	$duree = array_shift($args);
	$periode = array_shift($args);

	include_spip('action/editer_abonnement');
	return abonnement_modifier_echeance($id_abonnement, $duree, $periode, 'action/modifier_echeance_abonnement');
}