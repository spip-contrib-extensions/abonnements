<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Journaliser une action sur un abonnement : garde la trace de la date et de l'auteur de l'action
 * @param int $id_abonnement
 * @param string $action
 * @return void
 */
function abonnement_journaliser($id_abonnement, $action) {
	include_spip('inc/abonnements');
	$log = abonnement_log($action);

	sql_update('spip_abonnements', ['log' => 'concat(log,'.sql_quote($log).')'], 'id_abonnement='.intval($id_abonnement));
}

/**
 * Trouver un abonnement existant pour le même auteur et la même offre, pour un renouvellement éventuel
 * @param int $id_auteur
 * @param int $id_abonnements_offre
 * @param array $conditions
 * @return array|false
 */
function abonnement_trouver_existant_a_renouveler($id_auteur, $id_abonnements_offre, array $conditions = []) {
	// Si on trouve un abonnement de cette offre (le dernier en date)
	// et qu'il n'est pas trop vieux, ou sans de date de fin
	// et qu'on a pas forcé la création…
	$where_deja = [
		'id_auteur = ' . intval($id_auteur),
		'id_abonnements_offre = ' . intval($id_abonnements_offre),
	];

	while (count($conditions)) {
		$where_deja[] = array_shift($conditions);
	}

	$abonnements = [];
	foreach ([['prepa', 'actif'], ['inactif']] as $statuts) {
		$where_statuts = $where_deja;
		$where_statuts[] = sql_in('statut', $statuts);
		$abonnements_statuts = sql_allfetsel(
			'id_abonnement, date_debut, date_echeance, date_fin, echeance_duree, echeance_periode, statut',
			'spip_abonnements',
			$where_statuts,
			'',
			'maj desc',
			'0,100'
		);
		while (count($abonnements_statuts)) {
			$abonnements[] = array_shift($abonnements_statuts);
		}
	}

	return $abonnements;
}

/**
 * Créer ou renouveler un abonnement
 *
 * Si l'utilisateur n'a rien de cette offre, on crée un nouvel abonnement.
 * Si l'utilisateur a toujours ou avait précédemment un abonnement de cette offre, on le renouvelle.
 *
 * On s'assure d'avoir les droits pendant les modifs
 * car ce n'est pas un humain avec des droits qui déclanche ça explicitement
 *
 * @param int $id_auteur
 * 		Identifiant de l'utilisateur pour lequel on veut créer un abonnement
 * @param int $id_abonnements_offre
 * 		Identifiant de l'offre d'abonnement voulue
 * @param array $options
 *   bool $forcer_creation
 * 		`true` si on veut forcer la création sans chercher à renouveler
 *   int $id_commande
 *      pour lier l'abonnement a une commande
 *   string $raison
 *      pour journaliser la raison du renouvellement
 * @return mixed
 */
function abonnement_creer_ou_renouveler($id_auteur, $id_abonnements_offre, $options = []) {
	$forcer_creation = empty($options['forcer_creation']) ? false : $options['forcer_creation'];
	// Si on a bien un auteur et une offre
	if (
		($id_auteur = intval($id_auteur)) > 0
		and ($id_abonnements_offre = intval($id_abonnements_offre)) > 0
	) {
		include_spip('inc/config');
		include_spip('inc/autoriser');
		include_spip('action/editer_objet');
		include_spip('action/editer_liens');
		include_spip('inc/abonnements');

		// On cherche la durée limite pour renouveler un abonnement
		$heures_limite = lire_config('abonnements/renouvellement_heures_limite', 48);

		$abonnement = null;
		// si on est pas en creation forcee, chercher un abonnement existant à renouveller
		if (!$forcer_creation) {
			if (!empty($options['id_abonnement'])) {
				// si abonnement fourni explicitement en option, on veut renouveler celui-la
				$where_deja = [
					'id_abonnement='.intval($options['id_abonnement'])
				];
			}
			else {
				// Sinon on cherche un abonnement de cette offre (le dernier en date)
				// eventuellement lié à une commande connue
				// et qu'il n'est pas trop vieux, ou sans de date de fin
				$where_deja = [
					'statut != "poubelle"'
				];
				if (!empty($options['id_commande']) and defined('_DIR_PLUGIN_COMMANDES')) {
					// chercher les abonnements liés à la commande passée en option
					$liens = objet_trouver_liens(['commande' => $options['id_commande']], ['abonnement' => '*']);
					$id_abonnements = array_column($liens, 'abonnement');
					$where_deja[] = sql_in('id_abonnement', $id_abonnements);
				}
				// date limite pour le renouvellement
				$date_fin_maxi = date('Y-m-d H:i:s', strtotime('- ' . $heures_limite . ' hours'));
				$where_deja[] = "(date_fin='0000-00-00 00:00:00' OR date_fin>=".sql_quote($date_fin_maxi).")";
			}
			if ($abonnements = abonnement_trouver_existant_a_renouveler($id_auteur, $id_abonnements_offre, $where_deja)) {
				$abonnement = array_shift($abonnements);
			}
		}
		if ($abonnement and $id_abonnement = intval($abonnement['id_abonnement'])
		) {
			autoriser_exception('modifier', 'abonnement', $id_abonnement, true);
			autoriser_exception('instituer', 'abonnement', $id_abonnement, true);
			$raison = (empty($options['raison']) ? 'renouvellement' : $options['raison']);
			// si l'abonnement n'était plus actif (désactivé suite à retard de paiement ou paiement manqué ?) il faut le réactiver
			if ($abonnement['statut'] !== 'actif') {
				// On réactive l'abonnement
				objet_modifier('abonnement', $id_abonnement, ['statut' => 'actif']);
				abonnement_journaliser($id_abonnement, "Réactiver abonnement ($raison)");
			}
			// On le renouvelle !
			$retour = abonnement_modifier_echeance($id_abonnement, $abonnement['echeance_duree'], $abonnement['echeance_periode'], $raison);
			autoriser_exception('modifier', 'abonnement', $id_abonnement, false);
			autoriser_exception('instituer', 'abonnement', $id_abonnement, false);
			return $retour;
		}
		// Sinon on en crée un nouveau
		else {
			include_spip('action/editer_objet');
			autoriser_exception('creer', 'abonnement', '', true);
			if ($id_abonnement = objet_inserer('abonnement')) {
				$abonnement = sql_fetsel('*', 'spip_abonnements', 'id_abonnement='.intval($id_abonnement));
				if (!empty($options['id_commande']) and defined('_DIR_PLUGIN_COMMANDES')) {
					objet_associer(
						['commande' => $options['id_commande']],
						['abonnement' => $id_abonnement]
					);
				}
				autoriser_exception('creer', 'abonnement', '', false);
				autoriser_exception('modifier', 'abonnement', $id_abonnement, true);
				$abonnements_offre = sql_fetsel('*', 'spip_abonnements_offres', 'id_abonnements_offre=' . intval($id_abonnements_offre));

				// dans l'abonnement on veut stocker le prix de chaque echeance
				// le prix initial de la première échéance ne nous interesse pas vraiment
				// et on applique la taxe liée à l'offre, car le prix ttc est contractuel
				// si la taxe change du fait du legislateur, c'est le prix ht qui changera mais pas l'echeance
				$options_ht = [];
				if (!empty($options['prix_ht_echeance'])) {
					// on permet de passer un prix différent de celui en base ici, par exemple en cas de remise
					$options_ht['prix_ht_echeance'] = $options['prix_ht_echeance'];
				}
				$fonction_prix = charger_fonction('prix', 'inc/');
				$prix_ttc = $fonction_prix('abonnements_offre', $id_abonnements_offre, $options_ht);

				$set = [
					'id_auteur' => $id_auteur,
					'id_abonnements_offre' => $id_abonnements_offre,
					'echeance_duree' => $abonnements_offre['duree'],
					'echeance_periode' => $abonnements_offre['periode'],
					'echeance_prix'      => $prix_ttc,
				];
				if (!empty($options['bank_uid'])) {
					$set['bank_uid'] = $options['bank_uid'];
				}
				if (!empty($options['mode_paiement'])) {
					$set['mode_paiement'] = $options['mode_paiement'];
				}
				if (!empty($options['date_debut'])) {
					$abonnement['date_debut'] = $set['date_debut'] = $options['date_debut'];
				}
				$modifs_date = abonnements_initialisation_dates($abonnement, $abonnements_offre);
				$set = array_merge($set, $modifs_date);

				$raison = (empty($options['raison']) ? '' : '(' . $options['raison'] .') ');
				abonnement_journaliser($id_abonnement, "Initialisation abonnement $raison" . json_encode($set));

				$erreur = objet_modifier('abonnement', $id_abonnement, $set);
				autoriser_exception('modifier', 'abonnement', $id_abonnement, false);
				return [$id_abonnement, $erreur];
			}
		}
	}

	return false;
}

function abonnement_modifier_echeance($id_abonnement, $duree, $periode, $raison = '') {
	// Si on a bien un abonnement et qu'on a le droit de le modifier et qu'on a une durée != 0
	if (
		$duree = intval($duree)
		and $id_abonnement = intval($id_abonnement)
		and autoriser('modifier', 'abonnement', $id_abonnement)
		and $abonnement = sql_fetsel('date_debut, date_echeance, date_fin, ending', 'spip_abonnements', 'id_abonnement = ' . $id_abonnement)
	) {
		$jourdhui = date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME']);

		// Calculons la date de départ du renouvellement
		// Par défaut on part de la dernière échéance
		$date_depart = $abonnement['date_echeance'];

		// Si la date d'échéance n'était pas encore définie, on reprend depuis le début
		if ($date_depart == '0000-00-00 00:00:00') {
			$date_depart = $abonnement['date_debut'];
		}
		// Et si la date d'échéance était *déjà passée*, alors on renouvelle *à partir d'aujourd'hui* !
		elseif ($date_depart < $jourdhui) {
			$date_depart = $jourdhui;
		}

		// De combien doit-on modifier la date
		switch ($periode) {
			case 'heures':
				$ajout = " {$duree} hours";
				break;
			case 'jours':
				$ajout = " {$duree} days";
				break;
			case 'mois':
				$ajout = " {$duree} months";
				break;
			default:
				$ajout = '';
				break;
		}

		// Si la période existe
		if ($ajout) {
			$modifications = [];

			// Si la durée est positive, on ajoute un + devant (le - est déjà là pour les négatives)
			if ($duree > 0) {
				$ajout = ' +' . $ajout;
			}
			// Calcul de la date de fin
			$modifications['date_echeance'] = date('Y-m-d H:i:s', strtotime($date_depart . $ajout));

			// Si la date de fin n'est PAS infinie ET qu'elle se retrouve plus petite que l'échéance
			if (
				$abonnement['date_fin'] != '0000-00-00 00:00:00'
				and $abonnement['date_fin'] < $modifications['date_echeance']
			) {
				// On la remet à zero si c'est une fausse date de fin sans signification
				// (la date echeance dépassée suffit à déclencher une resiliation si besoin)
				if (empty($abonnement['ending'])) {
					$modifications['date_fin'] = '0000-00-00 00:00:00';
				}
				// on la decale jusqu'a la prochaine echeance si c'est un abonnement en fin de vie (resiliation demandee)
				else {
					$modifications['date_fin'] = $modifications['date_echeance'];
				}
			}

			if ($raison) {
				abonnement_journaliser($id_abonnement, "Modif echeance abonnement ($raison) :" . json_encode($modifications));
			}

			// On lance la modification
			include_spip('action/editer_objet');
			$erreur = objet_modifier('abonnement', $id_abonnement, $modifications);

			return [$id_abonnement, $erreur];
		}
	}

	spip_log("abonnement_modifier_echeance: echec modification abonnement #$id_abonnement (duree=$duree periode=$periode)", "abonnements" . _LOG_ERREUR);

	return null;
}

/**
 * Resilier un abonnement
 *   - a date future
 *   - ou immediatement si pas de date fournie (ou date dans le passé)
 *
 * @param int $id_abonnement
 * @param array $options
 *   ?string $date_fin date de fin ou 'echeance' pour resilier a la prochaine echeance (resiliation immediate si pas de date_fin)
 *   ?string $raison raison de la resiliation, pour garder une trace dans les logs de l'abonnement
 *   ?int ending_status : cas de resiliation, que l'on note dans l'abonnement pour trace et affichage
 *   ?bool notify_bank : un flag pour demander norification a la banque pour stoper les demandes de paiement recurrentes
 * @return ?array
 */
function abonnement_resilier($id_abonnement, $options) {

	$date_fin = (isset($options['date_fin']) ? $options['date_fin'] : null);
	$raison = (isset($options['raison']) ? $options['raison'] : "Résiliation " . ($date_fin ? " à $date_fin" : ''));
	$ending_status = (isset($options['ending_status']) ? $options['ending_status'] : 0);
	$notify_bank = (isset($options['notify_bank']) ? $options['notify_bank'] : false);

	if ($id_abonnement = intval($id_abonnement)
		and $abonnement = sql_fetsel('*', 'spip_abonnements', 'id_abonnement=' . intval($id_abonnement))) {
		include_spip('inc/autoriser');
		if (autoriser('resilier', 'abonnement', $id_abonnement)) {

			if ($date_fin === 'echeance') {
				$date_fin = $abonnement['date_echeance'];
			}

			include_spip('action/editer_objet');
			include_spip('inc/abonnements');
			// si c'est une resiliation immediate, on desactive directement
			if (is_null($date_fin) or strtotime($date_fin) < time()) {
				// on note que le ending_status si il y en a 1
				if ($ending_status) {
					$set = ['ending' => $ending_status,];
					objet_modifier('abonnement', $id_abonnement, $set);
				}
				abonnement_desactiver($id_abonnement, $raison);
				$abonnement = sql_fetsel('*', 'spip_abonnements', 'id_abonnement=' . intval($id_abonnement));
			}
			else {
				$set = [
					'date_fin' => $date_fin,
				];
				if ($ending_status and empty($abonnement['ending'])) {
					$set['ending'] = $ending_status;
				}
				objet_modifier('abonnement', $id_abonnement, $set);
				abonnement_journaliser($id_abonnement, $raison);
				abonnements_programmer_desactivation($id_abonnement, ['date_fin' => $date_fin]);
				$abonnement = array_merge($abonnement, $set);
			}

			// Si on détecte qu'il est lié à un prélèvement bancaire, on lance une résiliation par l'API
			if ($notify_bank and defined('_DIR_PLUGIN_BANK')) {
				if ($bank_uid = abonnement_trouver_bank_uid($id_abonnement)) {
					include_spip('abos/resilier');
					abos_resilier_notify_bank($bank_uid);
				}
			}

		} else {
			spip_log("abonnement_resilier: autorisation refusée pour #$id_abonnement", 'abonnements');
		}

		return $abonnement;
	}

	return null;
}


/**
 * Activer un abonnement en utilisant l'API et sans autorisation
 * car peut être appelée par un job queue
 *
 * @param int $id_abonnement
 * @param string $raison
 * @return void
 */
function abonnement_activer($id_abonnement, $raison = '') {
	include_spip('inc/autoriser');
	include_spip('action/editer_objet');
	// On inhibe les autorisations
	autoriser_exception('modifier', 'abonnement', $id_abonnement);
	autoriser_exception('instituer', 'abonnement', $id_abonnement);
	// On désactive l'abonnement
	objet_modifier('abonnement', $id_abonnement, ['statut' => 'actif']);
	if (empty($raison)) {
		$raison = "Activation abonnement";
	}
	abonnement_journaliser($id_abonnement, $raison);
	// On remet les autorisations
	autoriser_exception('instituer', 'abonnement', $id_abonnement, false);
	autoriser_exception('modifier', 'abonnement', $id_abonnement, false);
	// et dans la foulee, verifier qu'il ne faut pas *aussi* programmer sa desactivation si c'est un abonnement court par exemple
	abonnements_programmer_desactivation($id_abonnement);
}

/**
 * Désactiver un abonnement en utilisant l'API et sans autorisation
 * car peut être appelée par un job queue
 *
 * @param int $id_abonnement
 * @param string $raison
 * @return void
 */
function abonnement_desactiver($id_abonnement, $raison = '') {
	include_spip('inc/autoriser');
	include_spip('action/editer_objet');

	// verifier si cet abonnement est renouvelé par un autre, qu'on va activer immédiatement si besoin
	$ending = sql_getfetsel('ending', 'spip_abonnements', 'id_abonnement='.intval($id_abonnement));
	if ($ending < 0) {
		$abonnement_suivant = sql_fetsel('*', 'spip_abonnements', 'id_abonnement='.intval(-$ending));
		if ($abonnement_suivant
		  and $abonnement_suivant['statut'] === 'prepa'
		  and $abonnement_suivant['date_debut'] <= date('Y-m-d H:i:s')) {
			abonnement_activer($abonnement_suivant['id_abonnement'], "Activation en renouvellement abonnement #$id_abonnement terminé)");
		}
	}

	// On inhibe les autorisations
	autoriser_exception('modifier', 'abonnement', $id_abonnement);
	autoriser_exception('instituer', 'abonnement', $id_abonnement);
	// On désactive l'abonnement
	objet_modifier('abonnement', $id_abonnement, ['statut' => 'inactif']);
	if (empty($raison)) {
		$raison = "Désactivation abonnement";
	}
	abonnement_journaliser($id_abonnement, $raison);
	// On remet les autorisations
	autoriser_exception('instituer', 'abonnement', $id_abonnement, false);
	autoriser_exception('modifier', 'abonnement', $id_abonnement, false);
}


function abonnement_nettoyer_jobs_lies($id_abonnement, $fonction = null) {
	include_spip('action/editer_liens');

	// chercher les tâches liées à cet abonnement
	$liens = objet_trouver_liens(['job' => '*'], ['abonnement' => $id_abonnement]);
	if ($liens and is_array($liens)) {
		$id_jobs = array_column($liens, 'id_job');

		// si une (string) ou des (array) fonction on est fournies en argument, filtrer les jobs
		if (!empty($fonction)) {
			$id_jobs = sql_allfetsel('id_job', 'spip_jobs', [sql_in('id_job', $id_jobs), sql_in('fonction', is_array($fonction) ? $fonction : [$fonction])]);
			$id_jobs = array_column($id_jobs, 'id_job');
		}

		// Et on les supprime tous !
		if (!empty($id_jobs)) {
			array_map('job_queue_remove', $id_jobs);
		}
	}
}
