<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Action de renouveler un abonnement
 * @param int $id_abonnement
 */
function action_resilier_abonnement($id_abonnement = null, $immediat = false) {
	if (is_null($id_abonnement)) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
		$args = explode('-', $arg, 2);
		$id_abonnement = array_shift($args);
		if (!empty($args)) {
			$immediat = (bool)array_shift($args);
		}
	}

	if (
		$id_abonnement = intval($id_abonnement)
		and $id_abonnement > 0
		and include_spip('inc/autoriser')
		and autoriser('resilier', 'abonnement', $id_abonnement)
	) {

		$raison = 'Resiliation'
			. (test_espace_prive() ? " depuis ecrire/" : " depuis site public")
			. ($immediat ? ' (Immediat)' : ' (A echeance)');

		include_spip('inc/abonnements');
		include_spip('action/editer_abonnement');
		abonnement_resilier($id_abonnement, [
			'date_fin' => 'echeance',
			'raison' => $raison,
			'ending_status' => _ABONNEMENTS_ENDING_STATUS_DEMANDE_RESILIATION,
			'notify_bank' => true,
		]);

	}
}
