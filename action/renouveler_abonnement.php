<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) { return;
}

/**
 * Action de renouveler un abonnement
 * @param int $id_abonnement
 * @return ?array
 */
function action_renouveler_abonnement_dist($id_abonnement = null) {
	if (is_null($id_abonnement)) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$id_abonnement = $securiser_action();
	}

	// Si on a bien un abonnement et qu'on a le droit de le modifier
	if (
		$id_abonnement = intval($id_abonnement)
		and $id_abonnement > 0
		and autoriser('modifier', 'abonnement', $id_abonnement)
		and $abonnement = sql_fetsel('id_abonnements_offre, date_debut, date_fin, echeance_duree, echeance_periode', 'spip_abonnements', 'id_abonnement = ' . $id_abonnement)
		and $abonnement['echeance_duree'] > 0
		and $abonnement['echeance_periode']
	) {
		include_spip('action/editer_abonnement');
		return abonnement_modifier_echeance($id_abonnement, $abonnement['echeance_duree'], $abonnement['echeance_periode'], 'action/renouveler_abonnement');
	}

	return null;
}