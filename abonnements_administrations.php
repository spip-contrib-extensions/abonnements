<?php

/**
 * Plugin Abonnements
 * (c) 2012 Les Développements Durables
 * Licence GNU/GPL v3
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction d'installation du plugin et de mise à jour.
 * */
function abonnements_upgrade($nom_meta_base_version, $version_cible) {
	$maj = [];

	$maj['create'] = [['maj_tables', ['spip_abonnements_offres', 'spip_abonnements_offres_liens', 'spip_abonnements', 'spip_abonnements_offres_notifications']]];

	// Ajout de la config des notifications
	$maj['2.1.0'] = [
		['maj_tables', ['spip_abonnements_offres_notifications']]
	];

	// Ajout de la date d'échéance possiblement différente avec la date de fin
	$maj['2.2.0'] = [
		['maj_tables', ['spip_abonnements']],
		['sql_update', 'spip_abonnements', ['date_echeance' => 'date_fin']]
	];

	// Ajout des champs taxe et prix_ht, on copie la valeur de prix dans prix_ht
	$maj['2.2.2'] = [
		['maj_tables', ['spip_abonnements_offres']],
		['sql_alter', 'TABLE spip_abonnements_offres ADD prix_ht float(10,2) not null default 0 AFTER periode'],
		['sql_alter', 'TABLE spip_abonnements_offres ADD taxe decimal(4,4) null default 0 AFTER prix_ht'],
		['sql_update', 'spip_abonnements_offres', ['prix_ht' => 'prix']],
		['sql_update', 'spip_abonnements_offres', ['prix' => '0']]
	];

	// Nettoyage (d'une table inexistante...)
	$maj['2.2.3'] = [
		['sql_alter','TABLE spip_contacts_abonnements DROP prix'],
	];

	// relancer des abonnements après échéance
	$maj['2.2.4'] = [
		['sql_alter',"TABLE spip_abonnements_offres_notifications ADD `quand` ENUM('avant','apres') DEFAULT 'avant' NOT NULL AFTER `periode`"],
	];

	// Nettoyage : le champ `prix` est inutile, il suffit de `prix_ht` et `taxe`
	$maj['2.2.5'] = [
		['sql_alter','TABLE spip_abonnements_offres DROP prix'],
	];

	// Ajout d'une valeur possible au champ `quand`
	$maj['2.2.6'] = [
		['sql_alter',"TABLE spip_abonnements_offres_notifications CHANGE `quand` `quand` ENUM('avant','apres','pendant') DEFAULT 'avant' NOT NULL"]
	];

	// Ajout d'un champ immatériel pour savoir si c'est un service virtuel ou matériel
	$maj['2.3.0'] = [
		['maj_tables', ['spip_abonnements_offres']],
	];

	// Passage en décimal
	$maj['2.3.1'] = [
		['sql_alter', 'TABLE spip_abonnements_offres CHANGE prix_ht prix_ht DECIMAL(20,6) NOT NULL DEFAULT 0'],
	];

	// ajouter le champ pour gerer les fin d'abonnements (résiliation en cours, a date_fin)
	$maj['2.4.0'] = [
		['sql_alter', 'TABLE spip_abonnements ADD ending tinyint(1) DEFAULT 0 NOT NULL'],
	];
	// ajouter le champ log pour garder la trace de ce qui se passe sur un abonnement
	$maj['2.4.1'] = [
		['sql_alter', "TABLE spip_abonnements ADD log text NOT NULL DEFAULT ''"],
	];

	// ajouter les champs echeance_duree echeance_periode echeance_prix bank_uid mode_paiement
	$maj['2.5.0'] = [
		['sql_alter', "TABLE spip_abonnements ADD echeance_duree int(11) NOT NULL DEFAULT 0"],
		['sql_alter', "TABLE spip_abonnements ADD echeance_periode varchar(25) NOT NULL DEFAULT ''"],
		['sql_alter', "TABLE spip_abonnements ADD echeance_prix decimal(20,6) not null default 0"],
		['sql_alter', "TABLE spip_abonnements ADD bank_uid varchar(55) not null default ''"],
		['sql_alter', "TABLE spip_abonnements ADD mode_paiement varchar(25) NOT NULL DEFAULT ''"],
		['abonnements_completer_donnees_echeances']
	];

	$maj['2.5.1'] = [
		['sql_alter', "TABLE spip_abonnements ADD date_fin_mode_paiement datetime NOT NULL DEFAULT '0000-00-00 00:00:00'"],
	];
	$maj['2.5.2'] = [
		['abonnements_set_commandes_details_statut'],
	];

	$maj['2.5.3'] = [
		['sql_alter', 'TABLE spip_abonnements CHANGE ending ending bigint(21) DEFAULT 0 NOT NULL'],
	];

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Fonction de désinstallation du plugin.
 * */
function abonnements_vider_tables($nom_meta_base_version) {
	sql_drop_table('spip_abonnements_offres');
	sql_drop_table('spip_abonnements_offres_liens');
	sql_drop_table('spip_abonnements');
	sql_drop_table('spip_abonnements_offres_notifications');

	# Nettoyer les versionnages et forums
	sql_delete('spip_versions', sql_in('objet', ['abonnements_offre', 'abonnement']));
	sql_delete('spip_versions_fragments', sql_in('objet', ['abonnements_offre', 'abonnement']));
	sql_delete('spip_forum', sql_in('objet', ['abonnements_offre', 'abonnement']));

	effacer_meta($nom_meta_base_version);
}


function abonnements_completer_donnees_echeances() {
	$fonction_prix = charger_fonction('abonnements_offre', 'prix');
	include_spip('inc/autoriser');
	include_spip('inc/abonnements');
	include_spip('action/editer_liens');
	include_spip('action/editer_objet');

	$trouver_table = charger_fonction('trouver_table', 'base');
	$trouver_table(''); // vide le cache des desc pour prendre en compte les nouveaux champs de abonnements

	do {
		$res = sql_select('*', 'spip_abonnements', "echeance_periode=''", '', 'id_abonnement', '0,100');
		$nb = sql_count($res);
		spip_log("abonnements_completer_donnees_echeances: $nb abonnements à compléter", "maj");
		while ($row = sql_fetch($res)) {
			$id_abonnement = $row['id_abonnement'];
			if ($id_abonnements_offre = $row['id_abonnements_offre']
			  and $abonnements_offre = sql_fetsel('*', 'spip_abonnements_offres', 'id_abonnements_offre=' . intval($id_abonnements_offre))) {

				$prix_ht = $abonnements_offre['prix_ht'];
				$prix_ttc = $fonction_prix($id_abonnements_offre, $prix_ht);
				$set = [
					'echeance_duree' => $abonnements_offre['duree'],
					'echeance_periode' => $abonnements_offre['periode'],
					'echeance_prix'      => $prix_ttc,
				];
			}
			else {
				$set = ['echeance_periode' => '?'];
			}
			if ($id_commande = abonnement_trouver_id_commande($id_abonnement)
			  and $commande = sql_fetsel('*', 'spip_commandes', 'id_commande='.intval($id_commande))) {
				$set['bank_uid'] = $commande['bank_uid'];
				$set['mode_paiement'] = $commande['mode'];
			}
			autoriser_exception('modifier', 'abonnement', $id_abonnement, true);
			objet_modifier('abonnement', $id_abonnement, $set);
			$abonnement = sql_fetsel('*', 'spip_abonnements', 'id_abonnement='.intval($id_abonnement));
			if (empty($abonnement['echeance_periode']) or $abonnement['echeance_periode'] !== $set['echeance_periode']) {
				sql_updateq('spip_abonnements', $set, 'id_abonnement='.intval($id_abonnement));
				$abonnement = sql_fetsel('*', 'spip_abonnements', 'id_abonnement='.intval($id_abonnement));
			}
			if (empty($abonnement['echeance_periode']) or $abonnement['echeance_periode'] !== $set['echeance_periode']) {
				die("Echec mise à jour de l'abonnement #$id_abonnement : ". json_encode($set));
			}
			abonnement_journaliser($id_abonnement, "Upgrade base abonnement " . json_encode($set));

			if (time() > _TIME_OUT) {
				return;
			}
		}
	} while ($nb);

}

function abonnements_set_commandes_details_statut() {
	// pour toutes les commandes qui ont des transactions payées
	// passer les commandes_details de type abonnements_offfre en 'paye' si ils sont en 'attente'
	// sinon on va declencher des creations d'abonnements qui existent deja car on utilisait pas l'api distribuer avant
	// la version 4.1.0 du plugin abonnements
	$trouver_table = charger_fonction('trouver_table', 'base');
	if ($trouver_table('spip_transactions')
	  and $trouver_table('spip_commandes')) {
		if ($transactions = sql_allfetsel('id_commande', 'spip_transactions', "statut='ok' AND id_commande>0")) {
			$id_commandes = array_column($transactions, 'id_commande');
			$details = sql_allfetsel('id_commandes_detail', 'spip_commandes_details',
				"objet='abonnements_offre' and statut='attente' AND " . sql_in('id_commande', $id_commandes));
			$nb = count($details);
			$id_commandes_details = array_column($details, 'id_commandes_detail');
			spip_log($s = "abonnements: $nb commandes_details de type abonnements_offre sont encore en attente sur des commandes payées ".implode(', ', $id_commandes_details), 'maj');
			sql_updateq('spip_commandes_details', ['statut' => 'paye'], sql_in('id_commandes_detail', $id_commandes_details));
		}
	}
}