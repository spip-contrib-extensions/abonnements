<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function formulaires_configurer_abonnements_saisies_dist() {
	$saisies = [
		[
			'saisie' => 'choisir_objets',
			'options' => [
				'nom' => 'objets',
				'label' => _T('abonnementsoffre:configurer_objets_label'),
				'explication' => _T('abonnementsoffre:configurer_objets_explication'),
			],
		],
	];

	return $saisies;
}
