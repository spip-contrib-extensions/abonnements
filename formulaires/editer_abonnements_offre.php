<?php

/**
 * Plugin Abonnements
 * (c) 2012 Les Développements Durables
 * Licence GNU/GPL v3
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/actions');
include_spip('inc/editer');

/*
 * Déclaration des champs du formulaire
 */

function formulaires_editer_abonnements_offre_saisies_dist($id_abonnements_offre = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	$taxe_default = lire_config('produits/taxe', 0);
	return [
		[
			'saisie' => 'input',
			'options' => [
				'nom' => 'titre',
				'label' => _T('abonnementsoffre:champ_titre_label'),
				'obligatoire' => 'oui',
			],
		],
		[
			'saisie' => 'textarea',
			'options' => [
				'nom' => 'descriptif',
				'label' => _T('abonnementsoffre:champ_descriptif_label'),
				'rows' => 10,
				'inserer_barre' => 'edition',
			],
		],
		[
			'saisie' => 'input',
			'options' => [
				'nom' => 'duree',
				'label' => _T('abonnementsoffre:champ_duree_label'),
				'obligatoire' => 'oui',
				'defaut' => 0,
			],
			'verifier' => [
				'type' => 'entier',
				'options' => [
					'min' => 0,
				],
			],
		],
		[
			'saisie' => 'selection',
			'options' => [
				'nom' => 'periode',
				'label' => _T('abonnementsoffre:champ_periode_label'),
				'obligatoire' => 'oui',
				'cacher_option_intro' => 'oui',
				'data' => [
					'mois' => _T('abonnementsoffre:champ_periode_choix_mois'),
					'jours' => _T('abonnementsoffre:champ_periode_choix_jours'),
					'heures' => _T('abonnementsoffre:champ_periode_choix_heures'),
				],
				'defaut' => 'mois',
			],
		],
		[
			'saisie' => 'input',
			'options' => [
				'nom' => 'prix_ht',
				'label' => _T('abonnementsoffre:champ_prix_ht_label'),
				'obligatoire' => 'oui',
				'defaut' => 0,
			],
			'verifier' => [
				'type' => 'decimal',
				'options' => [
					'min' => 0,
				],
			],
		],
		[
			'saisie' => 'input',
			'options' => [
				'nom' => 'taxe',
				'label' => _T('abonnementsoffre:champ_taxe_label'),
				'obligatoire' => 'oui',
				'defaut' => $taxe_default,
			],
			'verifier' => [
				'type' => 'decimal',
				'options' => [
					'min' => 0,
					'max' => 100
				],
			],
		],
		[
			'saisie' => 'case',
			'options' => [
				'nom' => 'immateriel',
				'label' => _T('abonnementsoffre:champ_immateriel_label'),
				'label_case' => _T('abonnementsoffre:champ_immateriel_label_case'),
			],
		],
	];
}

/**
 * Identifier le formulaire en faisant abstraction des parametres qui ne representent pas l'objet edite
 */
function formulaires_editer_abonnements_offre_identifier_dist($id_abonnements_offre = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	return serialize([intval($id_abonnements_offre)]);
}

/**
 * Declarer les champs postes et y integrer les valeurs par defaut
 */
function formulaires_editer_abonnements_offre_charger_dist($id_abonnements_offre = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	$valeurs = formulaires_editer_objet_charger('abonnements_offre', $id_abonnements_offre, '', $lier_trad, $retour, $config_fonc, $row, $hidden);
	unset($valeurs['id_abonnements_offre']);
	if (empty($valeurs['taxe'])) {
		$valeurs['taxe'] = 0;
	} else {
		$valeurs['taxe'] = (floatval($valeurs['taxe']) * 100);
	}
	return $valeurs;
}

/**
 * Verifier les champs postes et signaler d'eventuelles erreurs
 */
function formulaires_editer_abonnements_offre_verifier_dist($id_abonnements_offre = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	return formulaires_editer_objet_verifier('abonnements_offre', $id_abonnements_offre, ['titre']);
}

/**
 * Traiter les champs postes
 */
function formulaires_editer_abonnements_offre_traiter_dist($id_abonnements_offre = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	$taxe = floatval(_request('taxe')) / 100;
	set_request('taxe', $taxe);
	return formulaires_editer_objet_traiter('abonnements_offre', $id_abonnements_offre, '', $lier_trad, $retour, $config_fonc, $row, $hidden);
}
