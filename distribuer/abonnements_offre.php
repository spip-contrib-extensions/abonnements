<?php

/**
 * Abonner un auteur
 *
 * @plugin     Abonnements
 * @copyright  2014
 * @author     cedric
 * @licence    GNU/GPL
 * @package    SPIP\Abos\API
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Distribuer une offre d'abonnement, c'est à dire créer un abonnement
 * @param $id_abonnements_offre
 * @param $detail
 * @param $commande
 * @return bool|string
 */
function distribuer_abonnements_offre_dist($id_abonnements_offre, $detail, $commande) {

	// Si on a pas encore envoye cet abonnement, c'est la premiere echeance
	if ($detail['statut'] == 'attente') {
		spip_log("distribuer_abonnements_offre_dist: creation d'un abonnement offre #$id_abonnements_offre pour commande #".$commande['id_commande'], 'abonnements' . _LOG_DEBUG);
		if (!empty($commande['bank_uid'])) {
			$bank_uid = $commande['bank_uid'];
		} else {
			$transaction = sql_fetsel('*', 'spip_transactions', 'statut=' . sql_quote('ok') . ' AND id_commande=' . intval($commande['id_commande']), '', 'id_transaction', '0,1');
			if ($transaction and !empty($transaction['abo_uid'])) {
				$bank_uid = $transaction['abo_uid'];
			}
			elseif (!empty($transaction['id_transaction'])) {
				$bank_uid = $transaction['id_transaction'];
			}
		}

		// c'est une création initiale d'abonnement
		include_spip('inc/abonnements');
		include_spip('action/editer_abonnement');
		$options = [
			'id_commande' => $commande['id_commande'],
			'forcer_creation' => true,
			'bank_uid' => $bank_uid,
			'mode_paiement' => $commande['mode'],
			'prix_ht_initial' => $detail['prix_unitaire_ht'], // reprendre le prix qui a ete enregistre dans la commande
		];
		if (isset($commande['echeances_date_debut']) and intval($commande['echeances_date_debut'])) {
			$options['date_debut'] = $commande['echeances_date_debut'];
		}

		// si c'est un abonnement sans renouvellement auto, chercher si on est pas en train de renouveller un abonnement du meme type, encore actif
		// avec date_fin dans le futur donc
		// auquel cas on démarre le nouvel abonnement à la date_fin de l'existant
		$abonnements_offre = sql_fetsel('*', 'spip_abonnements_offres', 'id_abonnements_offre=' . intval($id_abonnements_offre));
		if (empty($abonnements_offre['renouvellement_auto'])) {
			$where = [
				'date_fin>='.sql_quote(date('Y-m-d 00:00:00', $_SERVER['REQUEST_TIME'])),
				sql_in('statut', ['prepa', 'actif'])
			];
			$abonnements_deja = abonnement_trouver_existant_a_renouveler($commande['id_auteur'], $id_abonnements_offre, $where);
			if ($abonnements_deja) {
				$dates_fins = array_column($abonnements_deja, 'date_fin');
				array_multisort($dates_fins, SORT_DESC, $abonnements_deja);
				$date_debut = reset($dates_fins);
				if ($date_debut > date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME'])) {
					$options['date_debut'] = $date_debut;
					$abonnement_renouvele = reset($abonnements_deja);
					$options['id_abonnement_renouvele'] = $abonnement_renouvele['id_abonnement'];
				}
			}
		}

		// On crée ou renouvelle
		if ($retour = abonnement_creer_ou_renouveler($commande['id_auteur'], $id_abonnements_offre, $options)) {
			if (!empty($options['id_abonnement_renouvele'])) {
				[$id_abonnement, $erreur] = $retour;
				include_spip('action/editer_objet');
				objet_modifier('abonnement', $options['id_abonnement_renouvele'], ['ending' => -$id_abonnement]);
			}
			return 'envoye';
		}

	}
	// si on vient de 'attente_echeance' c'est un renouvellement
	elseif (!empty($commande['statut_ancien']) and $commande['statut_ancien'] === 'attente_echeance') {
		$forcer_creation = false;
		spip_log("distribuer_abonnements_offre_dist: renouvellement d'un abonnement offre #$id_abonnements_offre pour commande #".$commande['id_commande'], 'abonnements' . _LOG_DEBUG);

		// On crée ou renouvelle
		include_spip('inc/abonnements');
		include_spip('action/editer_abonnement');
		$options = [
			'id_commande' => $commande['id_commande'],
			'forcer_creation' => $forcer_creation,
			'mode_paiement' => $commande['mode'],
			'prix_ht_echeance' => $detail['prix_unitaire_ht'], // reprendre le prix qui a ete enregistre dans la commande
			'raison' => 'Paiement échéance',
		];
		if (!empty($commande['bank_uid'])) {
			$options['bank_uid'] = $commande['bank_uid'];
		}

		if ($retour = abonnement_creer_ou_renouveler($commande['id_auteur'], $id_abonnements_offre, $options)) {
			return 'envoye';
		}
	}

	return false;
}