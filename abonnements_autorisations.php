<?php

/**
 * Plugin Abonnements
 * (c) 2012 Les Développements Durables
 * Licence GNU/GPL v3
 */

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

// declaration vide pour ce pipeline.
function abonnements_autoriser() {
}


// -----------------
// Objet abonnements_offres


// bouton de menu
function autoriser_abonnementsoffres_menu_dist($faire, $type, $id, $qui, $opts) {
	return true;
}


// creer
function autoriser_abonnementsoffre_creer_dist($faire, $type, $id, $qui, $opt) {
	return $qui['statut'] == '0minirezo' and !$qui['restreint'];
}

// voir les fiches completes
function autoriser_abonnementsoffre_voir_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

// modifier
function autoriser_abonnementsoffre_modifier_dist($faire, $type, $id, $qui, $opt) {
	return $qui['statut'] == '0minirezo' and !$qui['restreint'];
}

// supprimer
function autoriser_abonnementsoffre_supprimer_dist($faire, $type, $id, $qui, $opt) {
	return $qui['statut'] == '0minirezo' and !$qui['restreint'];
}


// associer (lier / delier)
function autoriser_associerabonnementsoffres_dist($faire, $type, $id, $qui, $opt) {
	return $qui['statut'] == '0minirezo' and !$qui['restreint'];
}


// -----------------
// Objet abonnements


// creer
function autoriser_abonnement_creer_dist($faire, $type, $id, $qui, $opt) {
	return $qui['statut'] == '0minirezo' and !$qui['restreint'];
}

// voir les fiches completes
function autoriser_abonnement_voir_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

function autoriser_abonnement_renouveler_dist($faire, $type, $id, $qui, $opt) {
	if (!intval($id) or !$abonnement = sql_fetsel('*', 'spip_abonnements', 'id_abonnement='.intval($id))) {
        return false;
    }
    // on ne renouvelle pas si l'abonnement a deja été renouvelé par un autre abonnement
    if ($abonnement['ending'] < 0) {
        return false;
    }
    // on ne renouvelle pas un abonnement à la poubelle
    if ($abonnement['statut'] === 'poubelle') {
        return false;
    }
    // ni un abonnement qui est prepa, pas encore actif car renouvellement d'un abonnement existant
    if ($abonnement['statut'] === 'prepa') {
        return false;
    }
    // on ne renouvelle pas les abonnements inactifs trop vieux (les admins peuvent renouveler plus longtemps après l'échéance)
    if ($abonnement['statut'] === 'inactif') {
        $heures_limite = lire_config('abonnements/renouvellement_heures_limite', 48);
        if ($qui['statut'] == '0minirezo' and !$qui['restreint']) {
            $heures_limite *= 4;
        }
        $date_fin_maxi = date('Y-m-d H:i:s', strtotime('- ' . $heures_limite . ' hours'));
        if ($abonnement['date_fin'] < $date_fin_maxi) {
            return false;
        }
    }
    return true;
}

// modifier
function autoriser_abonnement_modifier_dist($faire, $type, $id, $qui, $opt) {
	return $qui['statut'] == '0minirezo' and !$qui['restreint'];
}

// supprimer
function autoriser_abonnement_supprimer_dist($faire, $type, $id, $qui, $opt) {
	return $qui['statut'] == '0minirezo' and !$qui['restreint'];
}

// resilier
function autoriser_abonnement_resilier_dist($faire, $type, $id, $qui, $opt) {
	// un admin peut resilier un abonnement
	if ($qui['statut'] == '0minirezo' and !$qui['restreint']) {
		return true;
	}
	// mais aussi le proprietaire d'un abonnement
	if (intval($id) and !empty($qui['id_auteur'])) {
		if (sql_countsel('spip_abonnements', ['id_abonnement='.intval($id), 'id_auteur='.intval($qui['id_auteur'])])) {
			return true;
		}
	}
	return false;
}