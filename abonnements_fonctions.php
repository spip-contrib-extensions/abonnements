<?php

/**
 * Fonctions utiles au plugin Abonnements
 *
 * @plugin     Abonnements
 * @copyright  2012-2020
 * @author     Les Développements Durables
 * @licence    GNU/GPL v3
 * @package    SPIP\Abonnements\Fonctions
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function filtre_abonnement_test_renouvellement_auto_dist($id_abonnement) {
    return '';
}

function filtre_abonnements_afficher_status_ending($ending, $statut = '', $date_fin = '', $texte_court = '') {
	if (empty($ending)) {
		return $texte_court;
	}

	if ($ending > 0) {
		$info = _T('abonnement:info_ending_status_' . $ending);
	} else {
		$id_abonnement_renouvellement = -$ending;
		$url_renouvele = generer_url_entite($id_abonnement_renouvellement, 'abonnement');
		$info = _T('abonnement:info_ending_status_renouvele', ['id' => $id_abonnement_renouvellement, 'url' => $url_renouvele]);
	}
	if ($statut === 'actif' and $date_fin) {
		include_spip('inc/filtres');
		$info .= ' (' . _T('abonnement:info_date_fin', ['date' => affdate($date_fin)]) .')';
	}
	switch ($ending) {
		case 2:
			$class = 'notice';
			break;
		case 1:
		case 3:
		default:
			$class = ($ending < 0) ? 'success' : 'info';
			break;
	}
	if ($texte_court) {
		return "<div class='msg-alert $class' title='".attribut_html($info)."'>$texte_court</div>";
	}
	else {
		return "<div class='msg-alert $class'>$info</div>";
	}
}
function filtre_abonnements_afficher_renouvellement_de($id_abonnement) {
    if ($previous = sql_fetsel('*', 'spip_abonnements', "ending=". intval(-$id_abonnement))) {
        $id_abonnement = $previous['id_abonnement'];
        $url = generer_url_entite($id_abonnement, 'abonnement');
        $date_fin = affdate($previous['date_fin']);
        $info = _T('abonnement:info_ending_status_renouvellement_de', ['id' => $id_abonnement, 'url' => $url, 'date_fin' => $date_fin]);
        return "<div class='msg-alert info'>$info</div>";
    }
    return '';
}
/**
 * Afficher la durée d'un abonnement en fonction d'une période
 *
 * Simplifie dans la mesure du possible, ex. :
 * 12 mois → 1 an
 * 24 heures → 1 jour
 *
 * @param Integer|String $duree
 * @param String $periode
 *     heure | jour | mois | an
 * @return string
 */
function filtre_abonnements_afficher_duree_dist($duree, $periode) {

	$texte = '';

	// Simplifions certaines durées
	$simplifier = [
		'heure' => ['modulo' => 24, 'periode' => 'jour'],
		'mois' => ['modulo' => 12, 'periode' => 'an'],
	];
	if (
		isset($simplifier[$periode])
		and (($duree % $simplifier[$periode]['modulo']) === 0)
	) {
		$duree /= $simplifier[$periode]['modulo'];
		$periode = $simplifier[$periode]['periode'];
	}

	// Singulier ou pluriel
	if ($duree == 1) {
		$texte = _T("abonnementsoffre:info_1_$periode");
	} else {
		if (substr($periode, -1, 1) !== 's') {
			$periode .= 's';
		}
		$texte = _T("abonnementsoffre:info_nb_$periode", ['nb' => $duree]);
	}

	return $texte;
}

/**
 * Un filtre pour afficher proprement la date debut/renouvellement/fin des abonnements
 * - si la date est vide ou nulle, on affiche rien
 * - si avec un horaire on utilise le format d/m/Y pour rester concis
 * - on affiche toujours l'année pour éviter toute ambiguité
 *
 * @param ?string $date
 * @param $horaire
 * @return string
 */
function filtre_abonnements_afficher_date_periode_dist($date, $horaire) {
	if (empty($date) or $date === '0000-00-00 00:00:00') {
		return '';
	}

	if ($horaire) {
		$date_array = recup_date($date);
		if (!$date_array) {
			return '';
		}

		[$annee, $mois, $jour, $heures, $minutes, $sec] = $date_array;

		return _T('date_fmt_jour_heure', [
			'jour' => affdate($date, "d/m/Y"),
			'heure' => _T('date_fmt_heures_minutes', ['h' => $heures, 'm' => $minutes])
		]);
	}

	return affdate($date);
}

function filtre_abonnements_affiche_transactions_liees_dist($bank_uid) {
	if (empty($bank_uid)) {
		return "<div class='msg-alert error'>Pas de bank_uid sur cet abonnement !</div>";
	}
	$out = '';
	if (defined('_DIR_PLUGIN_BANK')) {
		$id_transactions = sql_allfetsel('id_transaction', 'spip_transactions', 'abo_uid='.sql_quote($bank_uid));
		if (!empty($id_transactions)) {
			$id_transactions = array_column($id_transactions, 'id_transaction');
			$out .= recuperer_fond('prive/squelettes/inclure/liste-transactions', array(
				'id_transaction' => $id_transactions,
				'quoi' => 'abonnement',
			));
			include_spip('bank_fonctions');
			if (function_exists('bank_affiche_recurrences_de_transactions') and sql_countsel('spip_bank_recurrences')) {
				$out .= bank_affiche_recurrences_de_transactions($id_transactions);
			}
		}
	}
	return $out;
}