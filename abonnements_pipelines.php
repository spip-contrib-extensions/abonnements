<?php

/**
 * Plugin Abonnements
 * (c) 2012 Les Développements Durables
 * Licence GNU/GPL v3
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Optimiser la base de donnees des abonnements
 *
 * @param array $flux
 * @return array
 */
function abonnements_optimiser_base_disparus($flux) {

	//Offres d'abonnement à la poubelle
	$mydate = sql_quote(trim($flux['args']['date'], "'"));
	sql_delete('spip_abonnements_offres', "statut='poubelle' AND maj < $mydate");

	//Supprimer les abonnements lies à une offre d'abonnement inexistante
	$res = sql_select('DISTINCT abonnements.id_abonnements_offre', 'spip_abonnements AS abonnements
						LEFT JOIN spip_abonnements_offres AS offres
						ON abonnements.id_abonnements_offre=offres.id_abonnements_offre', 'offres.id_abonnements_offre IS NULL');
	while ($row = sql_fetch($res)) {
		sql_delete('spip_abonnements', 'id_abonnements_offre=' . $row['id_abonnements_offre']);
	}

	//Abonnements à la poubelle
	sql_delete('spip_abonnements', "statut='poubelle' AND maj < $mydate");

	include_spip('action/editer_liens');
	$flux['data'] += objet_optimiser_liens(['abonnement' => '*'], '*');
	return $flux;
}

/**
 * Des modifs supplémentaires après édition
 * @param array $flux
 * @return array
 * @throws JsonException
 */
function abonnements_post_edition($flux) {
	if (empty($flux['args']['table'])) {
		return $flux;
	}
	// Si on modifie un abonnement
	if ($flux['args']['table'] == 'spip_abonnements') {
		include_spip('inc/abonnements');
		$id_abonnement = intval($flux['args']['id_objet']);
		$abonnement = sql_fetsel('*', 'spip_abonnements', 'id_abonnement = ' . $id_abonnement);
		$jourdhui = date('Y-m-d H:i:s');


		include_spip('action/editer_abonnement');
		if (
			!empty($flux['data']['statut'])
			and in_array($flux['data']['statut'], ['inactif', 'poubelle'])
		) {
			// Si on a mis l'abonnement inactif ou à la poubelle, on doit enlever les tâches liées
			abonnement_nettoyer_jobs_lies($abonnement['id_abonnement']);
		} elseif (
			!empty($flux['data']['date_fin'])
		) {
			// Si la date de fin a été modifiée on le note
			abonnement_journaliser($id_abonnement, "Modification date_fin = " . $flux['data']['date_fin']);
			// Si elle est dans le futur on reprogramme la désactivation si besoin
			// si on vient d'annuler la date fin (modif d'un ancien abonnement), annuler le job de désactivation éventuellement existant
			if (!intval($flux['data']['date_fin']) or $flux['data']['date_fin'] > $jourdhui) {
				abonnements_programmer_desactivation($id_abonnement, ['date_fin' => $flux['data']['date_fin']]);
			}
		}

		if (isset($flux['data']['ending'])) {
			if ($flux['data']['ending'] < 0) {
				abonnement_journaliser($id_abonnement, "Renouvellement par abonnement #".(-$flux['data']['ending']));
			}
			else {
				abonnement_journaliser($id_abonnement, "Ending abonnement cause #".($flux['data']['ending']));
			}
		}

		// Si l'abonnement n'est pas deja lie a une commande,
		// et on a un id_commande dans l'environnement, on lie la commande à l'abonnement
		if (!$id_commande = abonnement_trouver_id_commande($id_abonnement)) {
			if (
					$id_commande = intval(_request('id_commande'))
					and defined('_DIR_PLUGIN_COMMANDES')
			) {
				// On lie cet abonnement avec la commande qui l'a généré
				include_spip('action/editer_liens');
				objet_associer(
					['commande' => $id_commande],
					['abonnement' => $id_abonnement]
				);
			}
		}

		// si c'est une mddif humaine dans l'interface qui touche le statut ou les dates, journaliser
		if ($_SERVER['REQUEST_METHOD'] === 'POST' and !empty($GLOBALS['visiteur_session']) and test_espace_prive()) {
			if (!empty($flux['data']['statut']) or !empty($flux['data']['date_fin']) or !empty($flux['data']['date_echeance'])) {
				abonnement_journaliser($id_abonnement, "Modification admin " . json_encode($flux['data']));
			}
		}

		$modifs = [];
		$modifs_instituer = [];

		// Si l'échéance est VIDE, et que pourtant l'offre parente A BIEN une durée
		// alors c'est qu'il faut initialiser les dates !
		// @deprecated : c'est abonnement_creer_ou_renouveler() qui initialise directement les dates
		if ($abonnement['date_echeance'] == '0000-00-00 00:00:00'
			and $offre = sql_fetsel('*', 'spip_abonnements_offres', 'id_abonnements_offre = ' . intval($abonnement['id_abonnements_offre']))
			and ( $offre['duree']) > 0) {
			$modifs = abonnements_initialisation_dates($abonnement, $offre);
			abonnement_journaliser($id_abonnement, "@deprecated Initialisation dates " . json_encode($modifs));
			// Si les dates doivent être changées, on change le tableau de l'abonnement pour le test de statut qui suivra
			if (isset($modifs['date_debut'])) {
				$abonnement['date_debut'] = $modifs['date_debut'];
			}
			if (isset($modifs['date_fin'])) {
				$abonnement['date_fin'] = $modifs['date_fin'];
			}
			if (isset($modifs['date_fin_mode_paiement'])) {
				$abonnement['date_fin_mode_paiement'] = $modifs['date_fin_mode_paiement'];
			}
		}

		// Seulement si personne n'a modifié le statut manuellement, alors on check les dates pour statufier
		// mais on ne touche pas aux abonnements à la poubelle
		if ($abonnement['statut'] !== 'poubelle' and
			(!isset($flux['data']['statut']) or !$flux['data']['statut'])) {
			spip_log("Post-édition : Statut ? " . json_encode(['id_abonnement' => $id_abonnement, 'statut' => $abonnement['statut'], 'jourdhui' => $jourdhui, 'date_debut' => $abonnement['date_debut'], 'date_echeance' => $abonnement['date_echeance']]), 'abonnements' . _LOG_DEBUG);
			// Si aujourd'hui est entre date_debut et date_echeance, on active
			if (
					$abonnement['statut'] !== 'actif'
					and $jourdhui >= $abonnement['date_debut']
					and $jourdhui <= $abonnement['date_echeance']
			) {
				$modifs_instituer['statut'] = 'actif';
				spip_log("Post-édition : passage de l’abonnement $id_abonnement en actif", 'abonnements.' . _LOG_INFO);
				spip_log($abonnement, 'abonnements.' . _LOG_INFO);
				abonnement_journaliser($id_abonnement, "Activation automatique abonnement");
			}
			// Si abonnement pas encore commence (date_debut dans le futur)
			elseif (
					$abonnement['statut'] !== 'prepa'
					and $jourdhui < $abonnement['date_debut']
			) {
				$modifs_instituer['statut'] = 'prepa';
				spip_log("Post-édition : passage de l’abonnement $id_abonnement en prepa", 'abonnements.' . _LOG_INFO);
				spip_log($abonnement, 'abonnements.' . _LOG_INFO);
				abonnement_journaliser($id_abonnement, "Mise en attente automatique abonnement");
			}
			// Si date_fin passee, on désactive
			// on ne teste pas date_echeance car ce sera à un génie de désactiver si trop dépassée
			elseif (
					$abonnement['statut'] !== 'inactif'
					and $abonnement['date_fin'] != '0000-00-00 00:00:00'
					and $jourdhui >= $abonnement['date_fin']
			) {
				$modifs_instituer['statut'] = 'inactif';
				spip_log("Post-édition : passage de l’abonnement $id_abonnement en inactif", 'abonnements.' . _LOG_INFO);
				spip_log($abonnement, 'abonnements.' . _LOG_INFO);
				abonnement_journaliser($id_abonnement, "Desactivation automatique abonnement");
			}
		}

		// S'il y a des modifs à faire on appelle l'API de modif
		if (!empty($modifs)) {
			include_spip('action/editer_objet');
			objet_modifier('abonnement', $flux['args']['id_objet'], $modifs);
		}
		// Pour le statut on fait à part (pb de double entrée avec Champs Extras)
		if (!empty($modifs_instituer)) {
			include_spip('action/editer_objet');
			objet_instituer('abonnement', $flux['args']['id_objet'], $modifs_instituer);
		}
	}

	return $flux;
}

/**
 * Activer un abonnement sur demande du plugin bank
 * ici on a en general pas besoin d'activer car deja fait de façon automatique par post_edition
 * mais on peut recuperer la date de validite du moyen de paiement
 *
 * @pipeline bank_abos_activer_abonnement
 * @param array $flux
 * @return array
 */
function abonnements_bank_abos_activer_abonnement($flux) {
	if (!empty($flux['args']['abo_uid'])) {
		$bank_uid = $flux['args']['abo_uid'];
		if ($abonnement = sql_fetsel('*', 'spip_abonnements', 'bank_uid='.sql_quote($bank_uid))) {
			$flux['data'] = $abonnement['id_abonnement'];
			$set = [];
			if (!empty($flux['args']['validite']) and intval($flux['args']['validite'])) {
				$set['date_fin_mode_paiement'] = $flux['args']['validite'];
			}
			if ($abonnement['statut'] !== 'actif') {
				$set['statut'] = 'actif';
			}
			if (!empty($set)) {
				abonnement_journaliser($abonnement['id_abonnement'], "Activation abonnement " . json_encode($set));
				include_spip('action/editer_objet');
				objet_modifier('abonnement', $abonnement['id_abonnement'], $set);
			}
		}
	}
	return $flux;
}


/**
 * Renouveler un abonnement sur demande du plugin bank
 * ici on ne gere pas le renouvelement, déclenché par distribuer()
 * mais on peut recuperer la date de validite du moyen de paiement et la mettre à jour
 *
 * @pipeline bank_abos_renouveler_abonnement
 * @param array $flux
 * @return array
 */
function abonnements_bank_abos_renouveler_abonnement($flux) {
	if (!empty($flux['args']['abo_uid'])) {
		$bank_uid = $flux['args']['abo_uid'];
		if ($abonnement = sql_fetsel('*', 'spip_abonnements', 'bank_uid='.sql_quote($bank_uid))) {
			$flux['data'] = $abonnement['id_abonnement'];
			$set = [];
			if (!empty($flux['args']['validite']) and intval($flux['args']['validite'])) {
				$set['date_fin_mode_paiement'] = $flux['args']['validite'];
			}

			if (!empty($set)) {
				abonnement_journaliser($abonnement['id_abonnement'], "Renouveler abonnement " . json_encode($set));
				include_spip('action/editer_objet');
				objet_modifier('abonnement', $abonnement['id_abonnement'], $set);
			}
		}
	}
	return $flux;
}


/**
 * Resilier les abonnements sur demande du plugin bank
 * - soit suite à un paiement refuse par la bank (erreur=true)
 * - soit car c'est la fin de la recurrence gérée par bank
 *
 * @pipeline bank_abos_resilier
 **/
function abonnements_bank_abos_resilier($flux) {
	// On commence par chercher la commande dont il s'agit
	// et trouver les abonnements lies
	if (
		defined('_DIR_PLUGIN_COMMANDES')
		and isset($flux['args']['id']) and $id = $flux['args']['id']
		and strncmp($id, 'uid:', 4) == 0
		and $bank_uid = substr($id, 4)
		and $commande = sql_fetsel('*', 'spip_commandes', 'bank_uid = ' . sql_quote($bank_uid))
		and $id_commande = intval($commande['id_commande'])
	) {

		include_spip('action/editer_liens');
		$liens_commandes = objet_trouver_liens(['commande' => $id_commande], ['abonnement' => '*']);
		if (!empty($liens_commandes)) {
			// On prend juste la première commande qu'on trouve
			$id_abonnements = array_column($liens_commandes, 'abonnement');

			include_spip('action/editer_abonnement');
			include_spip('inc/abonnements');
			$options = [
				'raison' => empty($flux['args']['message']) ? 'Demande plugin bank' : $flux['args']['message'],
				'ending_status' => (empty($flux['args']['erreur']) ? _ABONNEMENTS_ENDING_STATUS_PAIEMENTS_ARRETES : _ABONNEMENTS_ENDING_STATUS_ECHEC_PAIEMENT),
				'notify_bank' => empty($flux['args']['notify_bank']) ? false : $flux['args']['notify_bank'],
			];
			// si pas immediat, demander une resiliation a echeance
			if (empty($flux['args']['immediat'])) {
				$options['date_fin'] = 'echeance';
			}
			foreach ($id_abonnements as $id_abonnement) {
				abonnement_resilier($id_abonnement, $options);
			}
		}
	}

	return $flux;
}


/**
 * Ajout de tâches nécessaires aux abonnements
 *
 * - Une tâche pour vérifier toutes les heures si on a pas trop dépassé des échéances
 * - Une tâche pour vérifier toutes les heures si les abonnements actifs ont une tâche de désactivation
 * - Une tâche pour programmer les emails de notification à envoyer
 *
 * @pipeline taches_generales_cron
 * @param array $taches Liste des génies et leur périodicité
 * @return array Liste des tâches possiblement modifiées
 */
function abonnements_taches_generales_cron($taches) {
	$taches['abonnements_verifier_echeances'] = 60 * 60; // toutes les heures
	$taches['abonnements_verifier_activation'] = 60 * 60; // toutes les heures
	$taches['abonnements_verifier_desactivation'] = 60 * 60; // toutes les heures
	$taches['abonnements_verifier_notifications'] = 24 * 3600; // une fois par jour
	return $taches;
}

/**
 * Ajouter des choses dans la colonne de gauche
 *
 * Offres d'abonnements : config des notifications + notifications ponctuelles
 *
 * @param array $flux
 * @return array
 */
function abonnements_affiche_gauche($flux) {
	if (
		isset($flux['args']['exec'])
		and $flux['args']['exec'] == 'abonnements_offre'
		and isset($flux['args']['id_abonnements_offre'])
	) {
		$flux['data'] .= recuperer_fond(
			'prive/squelettes/navigation/inc-abonnements_notifications',
			[
				'id_abonnements_offre' => $flux['args']['id_abonnements_offre']
			]
		);
	}

	return $flux;
}

/**
 * Ajouter la boite des abonnements sur la fiche auteur
 * @param array $flux
 * @return array
 */
function abonnements_affiche_milieu($flux) {
	$e = trouver_objet_exec($flux['args']['exec']);

	// Sur la page des auteurs
	if (
		is_array($e)
		and $e['type'] == 'auteur'
		and $e['edition'] == false
	) {
		$id_auteur = $flux['args']['id_auteur'];

		$ins = recuperer_fond('prive/squelettes/inclure/abonnements_auteur', ['id_auteur' => $id_auteur]);
		if (($p = strpos($flux['data'], '<!--affiche_milieu-->')) !== false) {
			$flux['data'] = substr_replace($flux['data'], $ins, $p, 0);
		} else { $flux['data'] .= $ins;
		}
	}

	return $flux;
}

function abonnements_afficher_contenu_objet($flux){

	if ($flux['args']['type']=='commande'
	  AND $id_commande = $flux['args']['id_objet']){
		include_spip('action/editer_liens');
		$liens_commandes = objet_trouver_liens(['commande' => $id_commande], ['abonnement' => '*']);
		if (!empty($liens_commandes)) {
			$id_abonnement = array_column($liens_commandes, 'abonnement');
			$complement = recuperer_fond('prive/objets/liste/abonnements', ['id_abonnement' => $id_abonnement], ['ajax' => true]);
			$flux['data'] .= $complement;
		}
	}
	return $flux;
}

/**
 * Ajouter les offres sur les objets configurés
 * @param array $flux
 * @return array
 */
function abonnements_affiche_enfants($flux) {
	$e = trouver_objet_exec($flux['args']['exec']);

	// Sur la page d'un objet s'il fait partie de la config
	if (
		is_array($e)
		and !$e['edition']
		and in_array($e['table_objet_sql'], lire_config('abonnements/objets', []))
		and $texte = recuperer_fond(
			'prive/objets/editer/liens',
			[
				'table_source' => 'abonnements_offres',
				'objet' => $e['type'],
				'id_objet' => $flux['args']['id_objet']
			]
		)
	) {
		if ($p = strpos($flux['data'], '<!--affiche_milieu-->')) {
			$flux['data'] = substr_replace($flux['data'], $texte, $p, 0);
		} else {
			$flux['data'] .= $texte;
		}
	}

	return $flux;
}

/**
 * Afficher les contenus dans lesquels sont rangés les offres
 *
 * @param array $flux
 * @return array
 */
function abonnements_affiche_hierarchie($flux) {
	include_spip('inc/config');

	// Sur la page d'une offre
	if (
		$flux['args']['objet'] == 'abonnements_offre'
		and $objets = lire_config('abonnements/objets')
	) {
		include_spip('action/editer_liens');
		$objets = array_map('objet_type', $objets);

		// On cherche si cette offre à des liens
		if (
			$liens = objet_trouver_liens(
				['abonnements_offre' => $flux['args']['id_objet']],
				['*' => '*']
			)
		) {
			$liens_parents = [];
			$liens_offres = [];
			foreach ($liens as $lien) {
				// Seulement si ce lien est actuellement prévu dans la config
				if (in_array($lien['objet'], $objets)) {
					// Si c'est une liaison entre deux offres
					if ($lien['objet'] == 'abonnements_offre') {
						$liens_offres[] = "[->{$lien['objet']}{$lien['id_objet']}]";
					}
					else {
						$liens_parents[] = "[->{$lien['objet']}{$lien['id_objet']}]";
					}
				}
			}

			if ($liens_parents) {
				$liens_parents = PtoBR(propre(_T('abonnementsoffre:liens_parents_label') . join(', ', $liens_parents)));
				$flux['data'] .= '<div class="parents_offres">' . $liens_parents . '</div>';
			}
			if ($liens_offres) {
				$liens_offres = PtoBR(propre(_T('abonnementsoffre:liens_offres_label') . join(', ', $liens_offres)));
				$flux['data'] .= '<div class="liens_offres">' . $liens_offres . '</div>';
			}
		}
	}

	return $flux;
}

/**
 * Ajouter une feuille de style privée
 * @param string $flux
 * @return string
 */
function abonnements_header_prive($flux) {
	$flux = abonnements_insert_head($flux);
	return $flux;
}

/**
 * @param string $flux
 * @return string
 */
function abonnements_insert_head($flux) {
	$flux .= '<link rel="stylesheet" href="' . timestamp(find_in_path('css/abonnements_prive.css')). '" type="text/css" />';
	return $flux;
}