<?php

/**
 * Plugin Abonnements
 * (c) 2012 Les Développements Durables
 * Licence GNU/GPL v3
 */

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

// delai d'anticipation des jobs de desactivation : on ne lance un job de desactivation que pour les abonnements qui vont finir dans les prochaines 48h
defined('_ABONNEMENTS_ANTICIPATION_JOB_DESACTIVATION') || define('_ABONNEMENTS_ANTICIPATION_JOB_DESACTIVATION', 48 * 3600);
defined('_ABONNEMENTS_ENDING_STATUS_DEMANDE_RESILIATION') || define('_ABONNEMENTS_ENDING_STATUS_DEMANDE_RESILIATION', 1);
defined('_ABONNEMENTS_ENDING_STATUS_ECHEC_PAIEMENT') || define('_ABONNEMENTS_ENDING_STATUS_ECHEC_PAIEMENT', 2);
defined('_ABONNEMENTS_ENDING_STATUS_PAIEMENTS_ARRETES') || define('_ABONNEMENTS_ENDING_STATUS_PAIEMENTS_ARRETES', 3);


/**
 * Mise en forme du log d'une action sur un abonnement
 * @param $abo_log
 * @return string
 */
function abonnement_log($action) {
	$par = '';
	if (defined('_IS_CLI') AND _IS_CLI) {
		$par = _T('public:par_auteur') . ' [CLI]';
	} else if (!empty($GLOBALS['visiteur_session']['id_auteur'])) {
		$par = _T('public:par_auteur') . ' #' . $GLOBALS['visiteur_session']['id_auteur'] . ' ' . $GLOBALS['visiteur_session']['nom'];
	} else {
		$par = _T('public:par_auteur') . ' ' . $GLOBALS['ip'];
	}

	$log = date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME']) . ' | '
		. $par
		. ' : ' . $action . "\n--\n";
	return $log;
}

/**
 * Fonction pour trouver la commande liée à un abonnement
 * @param int $id_abonnement
 * @return int
 */
function abonnement_trouver_id_commande($id_abonnement) {
	$id_commande = 0;

	if (defined('_DIR_PLUGIN_COMMANDES')) {
		include_spip('action/editer_liens');
		$liens_commandes = objet_trouver_liens(['commande' => '*'], ['abonnement' => $id_abonnement]);
		if (!empty($liens_commandes)) {
			// On prend juste la première commande qu'on trouve
			$id_commande = array_column($liens_commandes, 'id_commande');
			$id_commande = reset($id_commande);
		}
	}

	return $id_commande;
}

/**
 * Fonction pour trouver le bank_uid associé à un abonnement
 * @param int $id_abonnement
 * @param ?int $id_commande
 * @return string
 */
function abonnement_trouver_bank_uid($id_abonnement, $id_commande = null) {
	$bank_uid = sql_getfetsel('bank_uid', 'spip_abonnements', 'id_abonnement='.intval($id_abonnement));
	if (empty($bank_uid) and defined('_DIR_PLUGIN_COMMANDES')) {
		if ($id_commande or $id_commande = abonnement_trouver_id_commande($id_abonnement)) {
			$commande = sql_fetsel('*', 'spip_commandes', 'id_commande=' . intval($id_commande));
			if (!empty($commande['bank_uid'])) {
				$bank_uid = $commande['bank_uid'];
			}
			elseif (defined('_DIR_PLUGIN_BANK')) {
				$bank_uid = sql_fetsel('abo_uid', 'spip_transactions', ['id_commande='.intval($id_commande), "abo_uid != ''", "statut='ok'"], '', 'date_transaction DESC');
			}
		}
	}
	return $bank_uid;
}

/**
 * Initialiser les dates d'échéance et de fin pour un abonnement créé
 *
 * @pipeline_appel abonnement_initialisation_dates
 * @param array $abonnement
 * 		Informations sur l'abonnement à initialiser
 * @param array $offre
 * 		Informations sur l'offre de l'abonnement à initialiser
 * @param int $id_commande
 * 		id_commande liée à l'abonnement
 * @return array
 * 		Retourne les modifications de dates initialisées
 **/
function abonnements_initialisation_dates($abonnement, $offre, $id_commande = 0) {
	$modifs = [];

	// De combien doit-on augmenter la date
	$duree = $offre['duree'];
	switch ($offre['periode']) {
		case 'heures':
			$ajout = " + {$duree} hours";
			break;
		case 'jours':
			$ajout = " + {$duree} days";
			break;
		case 'mois':
			$ajout = " + {$duree} months";
			break;
		default:
			$ajout = '';
			break;
	}

	// S'il n'y a pas eu de date de début déjà forcée à la création, alors on démarre l'abonnement maintenant
	if (empty($abonnement['date_debut'])
		or !intval($abonnement['date_debut'])) {
		$modifs['date_debut'] = $abonnement['date_debut'] = date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME']);
	}

	$modifs['date_echeance'] = date('Y-m-d H:i:s', strtotime($abonnement['date_debut'] . $ajout));

	// par défaut on mets une date_fin = date_echeance, c'est le plugin commandes_abonnements qui l'enleverra sur les
	// abos à renouvellement auto
	$modifs['date_fin'] = $modifs['date_echeance'];

	// Par défaut la date de fin reste vide car on a pas de fin prévue/connue

	$modifs = pipeline(
		'abonnement_initialisation_dates',
		[
			'args' => ['abonnement' => $abonnement, 'offre' => $offre],
			'data' => $modifs
		]
	);

	return $modifs;
}

/*
 * Programmer la désactivation d'un abonnement lors de sa date de fin
 *
 * @param int $id_abonnement
 *	L'identifiant de l'abonnement
 * @param array $options
 *   ?datetime $date_fin : Optionnellement la date de fin si on la connait déjà, ce qui évite une requête
 *   ?string $raison : raison de la desactivation, pour garder une trace dans les logs
 * @return bool
 */
function abonnements_programmer_desactivation($id_abonnement, $options = []) {
	include_spip('action/editer_liens');
	$id_abonnement = intval($id_abonnement);

	// Si on a pas de date, on va chercher
	if (!empty($options['date_fin'])) {
		$date_fin = $options['date_fin'];
	} else {
		$date_fin = sql_getfetsel('date_fin', 'spip_abonnements', 'id_abonnement = ' . $id_abonnement);
	}
	$raison = (empty($options['raison']) ? '' : $options['raison']);

	// dans tous les cas on nettoie la desactivation deja programmee le cas echeant
	include_spip('action/editer_abonnement');
	abonnement_nettoyer_jobs_lies($id_abonnement, ['abonnement_desactiver', 'abonnements_desactiver']);

	// si la date de desactivation est suffisament proche on programme la desactivation
	// mais pas trop loin dans le futur pour ne pas flooder la liste des jobs 5 ans à l'avance...
	if (intval($date_fin) AND strtotime($date_fin) < time() + _ABONNEMENTS_ANTICIPATION_JOB_DESACTIVATION) {

		// Seulement si on a bien une date de fin, on reprogramme, sans duplication possible
		$id_job = job_queue_add(
			'abonnement_desactiver',
			_T('abonnement:job_desactivation', ['id' => $id_abonnement]),
			[$id_abonnement, $raison],
			'action/editer_abonnement',
			true,
			strtotime($date_fin)
		);
		job_queue_link($id_job, ['objet' => 'abonnement', 'id_objet' => $id_abonnement]);
		return true;
	}

	return false;
}


/**
 * Envoyer un courriel à l'abonné pour lui rappeler une échéance.
 *
 * @example
 * Échéances dans 15 jours, il y a 1 mois, et le jour même :
 * ````
 * abonnements_notifier_echeance(1, 'untel', 'x@email.ltd', 15, 'jours', 'avant');
 * abonnements_notifier_echeance(1, 'untel', 'x@email.ltd', 1, 'mois', 'apres');
 * abonnements_notifier_echeance(1, 'untel', 'x@email.ltd', 0, 'jours', 'pendant');
 * ````
 *
 * @param int $id_abonnement
 *     Numéro de l'abonnement
 * @param string $nom
 *     Nom de la personne à notifier
 * @param string $email
 *     Email de la personne à notifier
 * @param int $duree
 *     Durée de l'échéance
 * @param string $periode
 *     Période de l'échéance : `jours` | `mois`
 * @param string $quand
 *     Indique si on est avant, après, ou le jour même de l'échéance
 *     - `avant`   : on est avant la fin de l'abonnement (par défaut pour rétro compat)
 *     - `après`   : on est après la fin de l'abonnement
 *     - `pendant` : on est le jour même de la fin de l'abonnement
 * @return void
 */
function abonnements_notifier_echeance($id_abonnement, $nom, $email, $duree, $periode, $quand = 'avant') {
	// Assurons nous que le "quand" est cohérent
	if ($duree === 0) {
		$quand = 'pendant';
	}
	$quoi    = 'abonnement_echeance';

	// envoyer la notif dans la langue de l'auteur, ou sinon dans la langue du site.
	$langue = ($id_auteur = sql_getfetsel('id_auteur', 'spip_abonnements', 'id_abonnement=' . $id_abonnement) and $langue_auteur = sql_getfetsel('lang', 'spip_auteurs', 'id_auteur=' . $id_auteur)) ? $langue_auteur : $GLOBALS['meta']['langue_site'] ;

	$options = [
		'nom'     => $nom,
		'email'   => $email,
		'duree'   => $duree,
		'periode' => $periode,
		'quand'   => $quand,
		'lang'    => $langue,
	];

	include_spip('action/editer_abonnement');
	$info = _T('abonnement:info_envoi_rappel_echeance') . ' ' . _T('abonnementsoffre:champ_periode_nb_' . $periode, ['nb' => ($quand == 'avant' ? '-' : '+') . $duree]);
	abonnement_journaliser($id_abonnement, "$info @ $email");

	$notifications = charger_fonction('notifications', 'inc');
	$notifications($quoi, $id_abonnement, $options);
}

/**
 * Lister tous les abonnements d'un utilisateur, classés par statut
 *
 * @param int $id_auteur
 *     Identifiant de l'utilisateur dont on cherche les abonnements
 * @return array
 *     Tableau des abonnements, rangés dans une clé pour chaque statut
 */
function abonnements_auteur_lister($id_auteur, $forcer = false) {
	static $abonnements_auteurs = [];
	$id_auteur = intval($id_auteur);

	if ($forcer || !isset($abonnements_auteurs[$id_auteur])) {
		$abonnements_auteurs[$id_auteur] = [];

		if ($abonnements = sql_allfetsel('*', 'spip_abonnements', 'id_auteur =' . $id_auteur)) {
			foreach ($abonnements as $abonnement) {
				$statut = $abonnement['statut'];

				// Initialiser pour ce statut
				if (!isset($abonnements_auteurs[$id_auteur][$statut])) {
					$abonnements_auteurs[$id_auteur][$statut] = [];
				}

				// Ajouter l'abonnement à ce statut
				$abonnements_auteurs[$id_auteur][$statut][] = $abonnement;
			}
		}
	}

	return $abonnements_auteurs[$id_auteur];
}


/**
 * @deprecated
 * @see abonnement_creer_ou_renouveler()
 */
function abonnements_creer_ou_renouveler($id_auteur, $id_abonnements_offre, $forcer_creation = false) {
	include_spip('action/editer_abonnement');
	$options = ['forcer_creation' => $forcer_creation];
	if ($id_commande = _request('id_commande')) {
		$options['id_commande'] = $id_commande;
	}
	return abonnement_creer_ou_renouveler($id_auteur, $id_abonnements_offre, $options);
}


/**
 * Désactiver un abonnement en utilisant l'API et sans autorisation
 * @deprecated
 *
 * @uses abonnement_desactiver()
 * @param $id_abonnement
 * @return void
 */
function abonnements_desactiver($id_abonnement) {
	include_spip('action/editer_abonnement');
	abonnement_desactiver($id_abonnement);
}
