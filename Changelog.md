# Plugin abonnements : notes de versions

## Unreleased

### Added

* #47 Ajouter une confirmation sur le bouton de renouvellement de l'abonnement

### Fixed

* #50 Calculer le prix avec l'API prix, qui gère l'arrondi automatiquement
* Suppression des balises `#DIV` dépréciées
* #48 Pas de page publique pour les abonnements

## v4.2.3

### Fixed

* #32 Syntaxe {$var} plutôt que ${var}

## v4.0.x

* Compatibilité SPIP 4.x, icones SVG

## v3.5.x

Ajout de squelettes de résumés génériques pour les abonnements et les offres :

* inclure/resume/abonnement.html + inc-abonnement_footer.html
* inclure/resume/abonnements_offre.html + inc-abonnements_offre_footer.html
