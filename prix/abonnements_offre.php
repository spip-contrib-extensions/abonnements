<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function prix_abonnements_offre_ht_dist($id_objet, $ligne, $options) {
	if(!empty($options['prix_ht_echeance'])) {
		return $options['prix_ht_echeance'];
	} else {
		return $ligne['prix_ht'] ?? 0;
	}
}

function prix_abonnements_offre_dist($id_objet, $prix_ht) {
	$prix = $prix_ht;

	// S'il y a une taxe de définie explicitement dans le produit, on applique en priorité
	if (
		($id_abonnements_offre = intval($id_objet)) > 0
			and include_spip('base/abstract_sql')
			and ( $taxe = sql_getfetsel('taxe', 'spip_abonnements_offres', 'id_abonnements_offre = ' . $id_abonnements_offre)) !== null
	) {
		$prix += $prix * $taxe;
	}
	// Sinon on applique la taxe par défaut
	else {
		include_spip('inc/config');
		$prix += $prix * lire_config('produits/taxe', 0);
	}

	return $prix;
}
