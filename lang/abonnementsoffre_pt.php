<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/abonnementsoffre-abonnements?lang_cible=pt
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_abonnementsoffre' => 'Adicionar esta modalidade de assinatura',

	// B
	'bouton_annuler' => 'Anular',
	'bouton_notifier' => 'Notificar',
	'bouton_verifier' => 'Verificar',

	// C
	'champ_ajouter_notification_label' => 'Adicionar uma notificação',
	'champ_descriptif_label' => 'Descrição',
	'champ_duree_0' => 'Sem limite',
	'champ_duree_label' => 'Duração',
	'champ_immateriel_label' => 'Imaterial',
	'champ_immateriel_label_case' => 'Esta assinatura corresponde a um serviço imaterial',
	'champ_periode_choix_heures' => 'Horas',
	'champ_periode_choix_jours' => 'Dias',
	'champ_periode_choix_mois' => 'Mês',
	'champ_periode_label' => 'Tipo de período',
	'champ_periode_nb_heures' => '@nb@ hora(s)',
	'champ_periode_nb_jours' => '@nb@ dia(s)',
	'champ_periode_nb_mois' => '@nb@ mês',
	'champ_prix_0' => 'Gratuito',
	'champ_prix_ht_label' => 'Preço sem IVA',
	'champ_prix_label' => 'Preço',
	'champ_prix_ttc_label' => 'Preço com IVA',
	'champ_quand_choix_apres' => 'Depois',
	'champ_quand_choix_avant' => 'Antes',
	'champ_quand_choix_pendant' => 'No mesmo dia',
	'champ_quand_label' => 'Quando?',
	'champ_taxe_label' => 'IVA (%)',
	'champ_titre_label' => 'Título',
	'configurer_notifications' => 'Configurar as notificações',
	'configurer_notifications_quand' => 'Quando?',

	// E
	'erreur_notification_doublon' => 'Esta notificação já está registada',
	'explication_configurer_notifications_abonnementsoffre' => 'Envio automático de notificações',
	'explication_envoyer_notifications_abonnementsoffre' => 'Envio imediato de notificações',

	// I
	'icone_creer_abonnementsoffre' => 'Criar uma modalidade de assinatura',
	'icone_modifier_abonnementsoffre' => 'Modificar esta modalidade de assinatura',
	'info_1_abonnementsoffre' => '1 modalidade de assinatura',
	'info_1_an' => '1 ano',
	'info_1_heure' => '1 hora',
	'info_1_jour' => '1 dia',
	'info_1_jours_apres' => '1 dia depois',
	'info_1_jours_avant' => '1 dia antes',
	'info_1_mois' => '1 mês',
	'info_1_mois_apres' => '1 mês depois',
	'info_1_mois_avant' => '1 mês antes',
	'info_abonnementsoffres_auteur' => 'As modalidades de assinatura deste autor',
	'info_aucun_abonnementsoffre' => 'Nenhuma modalidade de assinatura',
	'info_aucune_notification' => 'Nenhuma notificação',
	'info_nb_abonnementsoffres' => '@nb@ modalidades de assinatura',
	'info_nb_ans' => '@nb@ anos',
	'info_nb_heures' => '@nb@ horas',
	'info_nb_jours' => '@nb@ jours',
	'info_nb_jours_apres' => '@nb@ dias depois',
	'info_nb_jours_avant' => '@nb@ dias antes',
	'info_nb_mois' => '@nb@ meses',
	'info_nb_mois_apres' => '@nb@ meses depois',
	'info_nb_mois_avant' => '@nb@ meses antes',

	// M
	'message_notifier_ok' => 'As notificações foram enviadas.',
	'message_notifier_verifier_abonnements' => 'Verifique os utilizadores à direita antes de confirmar o envio de notificações.',

	// N
	'nb_abonnements' => 'Nb. abo.',

	// R
	'retirer_lien_abonnementsoffre' => 'Remover esta modalidade de assinatura',
	'retirer_tous_liens_abonnementsoffres' => 'Remover todas as modalidades de assinatura',

	// S
	'statut_prive' => 'privada',
	'statut_publie' => 'publicada em linha',

	// T
	'texte_ajouter_abonnementsoffre' => 'Adicionar uma modalidade de assinatura',
	'texte_changer_statut_abonnementsoffre' => 'Esta modalidade de assinatura é:',
	'texte_creer_associer_abonnementsoffre' => 'Criar e associar uma modalidade de assinatura',
	'titre_abonnementsoffre' => 'Modalidade de assinatura',
	'titre_abonnementsoffres' => 'Modalidades de assinatura',
	'titre_abonnementsoffres_rubrique' => 'Modalidades de assinatura desta secção',
	'titre_langue_abonnementsoffre' => 'Língua desta modalidade de assinatura',
	'titre_logo_abonnementsoffre' => 'Logotipo desta modalidade de assinatura',
	'titre_notifier_abonnementsoffre' => 'Enviar notificações'
);
