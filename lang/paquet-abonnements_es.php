<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-abonnements?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'abonnements_description' => 'Este plugin tiene el objetivo de regrupar todo lo que es común a los diferentes tipos de inscripciones posibles (zonas restringidas, contenidos precisos, o porque no ...  a una versión papel...).

Permite de definir las inscripciones propuestas por el sitio, y gestiona las personas que se han inscrito , desactivando la inscripción al cabo de un cierto tiempo.

Como pueden haber varios casos diferentes, no es este plugin el que define los derechos que son dados cuando se hace una inscripción. Son otros plugins los que van a implementar esto, por ejemplo para vincular un utilizador a una zona restringida.
',
	'abonnements_nom' => 'Inscripciones ',
	'abonnements_slogan' => 'Dar derechos durante un cierto tiempo'
);
