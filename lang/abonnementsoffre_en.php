<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/abonnementsoffre-abonnements?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_abonnementsoffre' => 'Add this subscription offer',

	// B
	'bouton_annuler' => 'Cancel',
	'bouton_notifier' => 'Notify',
	'bouton_verifier' => 'Verify',

	// C
	'champ_ajouter_notification_label' => 'Add a notification',
	'champ_descriptif_label' => 'Description',
	'champ_duree_0' => 'No limit',
	'champ_duree_label' => 'Duration',
	'champ_immateriel_label' => 'Dematerialized',
	'champ_immateriel_label_case' => 'This offer corresponds to a dematerialized service',
	'champ_periode_choix_annee' => 'Year',
	'champ_periode_choix_heures' => 'Hours',
	'champ_periode_choix_jours' => 'Days',
	'champ_periode_choix_mois' => 'Month',
	'champ_periode_label' => 'Period type',
	'champ_periode_nb_heures' => '@nb@ hour(s)',
	'champ_periode_nb_jours' => '@nb@ day(s)',
	'champ_periode_nb_mois' => '@nb@ months',
	'champ_prix_0' => 'Free',
	'champ_prix_ht_label' => 'Price excl. tax',
	'champ_prix_label' => 'Price',
	'champ_prix_ttc_label' => 'Price incl. tax',
	'champ_quand_choix_apres' => 'After',
	'champ_quand_choix_avant' => 'Before',
	'champ_quand_choix_pendant' => 'The same day',
	'champ_quand_label' => 'When ?',
	'champ_taxe_label' => 'VAT (%)',
	'champ_titre_label' => 'Title',
	'configurer_notifications' => 'Configure the notifications',
	'configurer_notifications_quand' => 'When ?',
	'configurer_objets_explication' => 'You will be able to sort the subscription offers in the pages of the following contents.',
	'configurer_objets_label' => 'Sort offers in content',
	'configurer_titre' => 'Configure subscriptions',

	// E
	'erreur_notification_doublon' => 'This notification is already registered',
	'explication_configurer_notifications_abonnementsoffre' => 'Automatic sending of notifications.',
	'explication_envoyer_notifications_abonnementsoffre' => 'Send notifications immediately.',

	// I
	'icone_creer_abonnementsoffre' => 'Create a subscription offer',
	'icone_modifier_abonnementsoffre' => 'Modify this subscription offer',
	'info_1_abonnementsoffre' => '1 subscription offer',
	'info_1_an' => '1 year',
	'info_1_heure' => '1 hour',
	'info_1_jour' => '1 day',
	'info_1_jours_apres' => '1 day after',
	'info_1_jours_avant' => '1 day before',
	'info_1_mois' => '1 month',
	'info_1_mois_apres' => '1 month after',
	'info_1_mois_avant' => '1 month before',
	'info_abonnementsoffres_auteur' => 'Subscription offers of this author',
	'info_aucun_abonnementsoffre' => 'No subscription offer',
	'info_aucune_notification' => 'No notification',
	'info_nb_abonnementsoffres' => '@nb@ subscription offers',
	'info_nb_ans' => '@nb@ years',
	'info_nb_heures' => '@nb@ hours',
	'info_nb_jours' => '@nb@ days',
	'info_nb_jours_apres' => '@nb@ days after',
	'info_nb_jours_avant' => '@nb@ days before',
	'info_nb_mois' => '@nb@ months',
	'info_nb_mois_apres' => '@nb@ month after',
	'info_nb_mois_avant' => '@nb@ month before',

	// L
	'liens_offres_label' => 'Linked to subscription offers:',
	'liens_parents_label' => 'In the contents:',

	// M
	'message_notifier_ok' => 'Notifications have been successfully sent.',
	'message_notifier_verifier_abonnements' => 'Check the users before validating the sending of notifications.',

	// R
	'retirer_lien_abonnementsoffre' => 'Remove this subscription offer',
	'retirer_tous_liens_abonnementsoffres' => 'Remove all subscription offers',

	// S
	'statut_prive' => 'private',
	'statut_publie' => 'published',

	// T
	'texte_ajouter_abonnementsoffre' => 'Add a subscription offer',
	'texte_changer_statut_abonnementsoffre' => 'This subscription offer is:',
	'texte_creer_associer_abonnementsoffre' => 'Create and associate a subscription offer',
	'titre_abonnementsoffre' => 'Subscription offer',
	'titre_abonnementsoffres' => 'Subscription offers',
	'titre_abonnementsoffres_rubrique' => 'Subscription offers of the section',
	'titre_langue_abonnementsoffre' => 'Language in this subscription offer',
	'titre_logo_abonnementsoffre' => 'Logo of this subscription offer',
	'titre_notifier_abonnementsoffre' => 'Send notifications'
);
