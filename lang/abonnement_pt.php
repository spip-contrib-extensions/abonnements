<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/abonnement-abonnements?lang_cible=pt
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_abonnement' => 'Adicionar esta assinatura',

	// C
	'champ_date_au_label' => 'a',
	'champ_date_debut_label' => 'Início da assinatura',
	'champ_date_du_label' => 'De',
	'champ_date_echeance_label' => 'Próxima data de expiração',
	'champ_date_fin_allonger_label' => 'Pode modificar a data de fim',
	'champ_date_fin_label' => 'Fim da assinatura',
	'champ_dates_debut_label' => 'Início das assinaturas',
	'champ_dates_fin_label' => 'Fim das assinaturas',
	'champ_echeance_duree_label' => 'Duração',
	'champ_id_abonnements_offre_label' => 'Modalidade de assinatura',
	'champ_id_auteur_label' => 'Utilizador',
	'champ_notifier_statut_label' => 'Situação das assinaturas',

	// E
	'erreur_id_abonnements_offre' => 'É necessário criar uma assinatura para uma modalidade existente.',

	// I
	'icone_creer_abonnement' => 'Criar uma assinatura',
	'icone_modifier_abonnement' => 'Modificar esta assinatura',
	'icone_renouveler_abonnement' => 'Renovar esta assinatura',
	'info_1_abonnement' => '1 assinatura',
	'info_1_abonnement_actif' => '1 assinatura activa',
	'info_1_abonnement_inactif' => '1 assinatura inactiva',
	'info_1_abonnement_notifier' => '1 assinatura a notificar',
	'info_abonnements_auteur' => 'As assinaturas deste autor',
	'info_aucun_abonnement' => 'Nenhuma assinatura',
	'info_aucun_abonnement_actif' => 'Nenhuma assinatura activa',
	'info_aucun_abonnement_inactif' => 'Nenhuma assinatura inactiva',
	'info_aucun_abonnement_notifier' => 'Nenhuma assinatura a notificar',
	'info_date_fin' => 'fim em @date@',
	'info_nb_abonnements' => '@nb@ assinaturas',
	'info_nb_abonnements_actifs' => '@nb@ assinaturas activas',
	'info_nb_abonnements_inactifs' => '@nb@ assinaturas inactivas',
	'info_nb_abonnements_notifier' => '@nb@ assinaturas a notificar',
	'info_numero_abbr' => 'n°',
	'info_numero_abbr_maj' => 'N°',

	// J
	'job_desactivation' => 'Desactivação da assinatura @id@',

	// L
	'label_date_a_partir' => 'A partir de ',
	'label_date_depuis' => 'Desde ',
	'label_dates' => 'Datas',
	'label_duree' => 'Duração',
	'label_montant' => 'Montante',
	'label_statut' => 'Estatuto',

	// N
	'notification_echeance_chapo' => '<p>Olá @nom@,</p>',
	'notification_echeance_corps' => '<p>Olá @nom@,</p>
		<p>Recebeu este e-mail porque tem uma assinatura no @nom_site_spip@ com a modalidade "@offre@".</p>
		<p>Esta assinatura expira em <strong>@echeance@</strong>.<br/>
		Convidamo-lo/a, portanto, a renová-la antes que esta expire.</p>
		<p>Obrigado pela sua confiança, e por favor não hesite em contactar-nos para qualquer informação adicional.</p>',
	'notification_echeance_corps_apres' => '<p>Recebeu este email porque tem uma assinatura no sítio @nom_site_spip@ com a modalidade « @offre@ ».</p>
	<p>Esta assinatura expirou há:  <strong>@echeance@</strong>.<br/>
	Convidamo-lo/a pois, a renová-la.</p>',
	'notification_echeance_corps_avant' => '<p>Recebeu este email porque tem uma assinatura no sítio @nom_site_spip@ com a modalidade " @offre@ ".</p>
	<p>Esta assinatura expira em:  <strong>@echeance@</strong>.<br/>
	Convidamo-lo/a pois, a renová-la antes da sua data de fim.</p>',
	'notification_echeance_corps_pendant' => '<p>Recebeu este email porque tem uma assinatura no sítio @nom_site_spip@ com a modalidade « @offre@ ».</p>
	<p>Esta assinatura expira hoje.<br/>
	Convidamo-lo/a pois, a renová-la antes da sua data de fim.</p>',
	'notification_echeance_signature' => '<p>Obrigado pela sua confiança, e por favor não hesite em contactar-nos para qualquer informação adicional.</p>',
	'notification_echeance_sujet_jours_apres' => 'A sua assinatura expirou há @duree@ dia(s) !',
	'notification_echeance_sujet_jours_avant' => 'A sua assinatura expira em @duree@ dia(s) !',
	'notification_echeance_sujet_jours_pendant' => 'A sua assinatura expira hoje!',
	'notification_echeance_sujet_mois_apres' => 'A sua assinatura expirou há @duree@ meses !',
	'notification_echeance_sujet_mois_avant' => 'A sua assinatura expira em @duree@ meses !',
	'notification_echeance_sujet_mois_pendant' => 'A sua assinatura expira este mês!',

	// R
	'retirer_lien_abonnement' => 'Remover esta assinatura',
	'retirer_tous_liens_abonnements' => 'Remover todas as assinaturas',

	// S
	'statut_actif' => 'activo',
	'statut_actifs' => 'activas',
	'statut_inactif' => 'desactivado',
	'statut_inactifs' => 'desactivadas',
	'statut_tous' => 'todas',

	// T
	'texte_ajouter_abonnement' => 'Adicionar uma assinatura',
	'texte_changer_statut_abonnement' => 'Esta assinatura é:',
	'texte_creer_associer_abonnement' => 'Criar e associar uma assinatura',
	'titre_abonnement' => 'Assinatura',
	'titre_abonnements' => 'Assinaturas',
	'titre_abonnements_rubrique' => 'Assinaturas da secção',
	'titre_abonnements_suivre' => 'Seguir as assinaturas',
	'titre_langue_abonnement' => 'Língua desta assinatura',
	'titre_logo_abonnement' => 'Logo desta assinatura'
);
