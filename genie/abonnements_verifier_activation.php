<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) { return;
}

/*
 * Activer les abonnements ayant une date_debut dépassée mais pas encore actif
 */
function genie_abonnements_verifier_activation_dist($time) {
	include_spip('base/abstract_sql');

	$maintenant = date('Y-m-d H:i:s');
	// On va chercher tous les abonnements actifs ayant une date de debut, dépassée
	// pour les activer immediatement
	$where_a_demarrer = [
		sql_in('statut', ['actif', 'poubelle'], 'NOT'),
		'date_debut<'.sql_quote($maintenant),
		'(date_fin='.sql_quote('0000-00-00 00:00:00') .' OR date_fin>'.sql_quote($maintenant).')',
	];
	$a_demarrer = sql_allfetsel('id_abonnement', 'spip_abonnements', $where_a_demarrer);
	if (count($a_demarrer)) {
		$a_demarrer = array_column($a_demarrer, 'id_abonnement');
		include_spip('action/editer_abonnement');
		array_map('abonnement_activer', $a_demarrer);
	}

	// TODO ? programmer une tache pour une activation précise des abonnements commençant dans les prochaines 48h ?

	return 1;
}