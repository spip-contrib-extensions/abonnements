<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) { return;
}

/*
 * Vérifie si les abonnements actifs ont bien une tâche précise de désactivation
 */
function genie_abonnements_verifier_desactivation_dist($time) {
	include_spip('base/abstract_sql');

	$maintenant = date('Y-m-d H:i:s');
	// On va chercher tous les abonnements actifs ayant une date de fin, dépassée
	// pour les terminer immediatement
	$where_termines = [
		sql_in('statut', ['inactif', 'poubelle'], 'NOT'),
		'date_fin>'.sql_quote('0000-00-00 00:00:00'),
		'date_fin<'.sql_quote($maintenant)
	];
	$termines = sql_allfetsel('id_abonnement', 'spip_abonnements', $where_termines);
	if (count($termines)) {
		$termines = array_column($termines, 'id_abonnement');
		include_spip('action/editer_abonnement');
		array_map('abonnement_desactiver', $termines);
	}

	// On va chercher tous les abonnements actifs ayant une date de fin dans les prochaines 48h
	// pour programmer leur desactivation (plus précise)
	include_spip('inc/abonnements');
	$maintenant_plus_anticipation = date('Y-m-d H:i:s', time() + _ABONNEMENTS_ANTICIPATION_JOB_DESACTIVATION);
	if (
		$a_changer = sql_allfetsel(
			'id_abonnement, date_fin',
			'spip_abonnements as a' .
			' LEFT JOIN spip_jobs_liens AS l ON l.objet = "abonnement" AND l.id_objet = a.id_abonnement' .
			' LEFT JOIN spip_jobs AS j ON j.fonction = "abonnement_desactiver" AND j.id_job = l.id_job',
			[
			'a.statut = "actif"',
			'a.date_fin > ' . sql_quote('0000-00-00 00:00:00'),
			'a.date_fin < ' . sql_quote($maintenant_plus_anticipation),
			'l.id_job IS NULL',
			]
		) and is_array($a_changer)
	) {
		foreach ($a_changer as $abonnement) {
			abonnements_programmer_desactivation($abonnement['id_abonnement'], $abonnement['date_fin']);
		}
	}

	return 1;
}